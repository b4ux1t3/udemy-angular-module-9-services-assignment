import { Injectable } from '@angular/core';
import { LoggingService } from './logging.service';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
    activations : number = 0;
    deactivations : number = 0;
  constructor(private logger : LoggingService) { }

  activation() : number {
    this.logger.log('Counter', `${++this.activations} activations so far.`);
    return this.activations;
  }

  deactivation() : number {
    this.logger.log('Counter', `${++this.deactivations} deactivations so far.`);
      return this.deactivations;
  }

}
