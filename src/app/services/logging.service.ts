import { Injectable } from "@angular/core";

@Injectable()
export class LoggingService {
    private date : Date = new Date();
    logStatusChange(status : string){
        console.log('A server status changed, new status:', status);
    }

    currentLogId : number = 0;
    log(caller : string, message : string){
        this.currentLogId++;
        let currentTime = this.date.toISOString();
        let splitString = message.split("\n");
        splitString.forEach(line => {
            console.log(`${this.currentLogId}\t${currentTime} - ${caller}: ${line}`);
        });
    }
} 