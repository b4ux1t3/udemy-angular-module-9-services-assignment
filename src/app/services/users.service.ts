import { Injectable } from '@angular/core';
import { CounterService } from './counter.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
    inactiveUsers : string[] = ['Joe', 'MaryAnn', 'Kingsley'];
    activeUsers : string[] = ['Kimberly', 'Ezreal', 'Reginald'];
  constructor(private counter : CounterService) { }

  toggleUserActive(id : number) : void {
    if (id < this.inactiveUsers.length) {
        this.activeUsers.push(this.inactiveUsers.splice(id, 1)[0]);
        this.counter.activation();
    }
  }
  toggleUserInactive(id : number) : void {
      this.inactiveUsers.push(this.activeUsers.splice(id, 1)[0]);
      this.counter.deactivation();
  }
}
